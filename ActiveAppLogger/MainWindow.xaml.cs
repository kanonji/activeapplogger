﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Threading;

namespace ActiveAppLogger
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer timer;
        private ObservableCollection<ActiveAppLog> log;

        public MainWindow()
        {
            InitializeComponent();
            this.timer = new DispatcherTimer();
            this.timer.Interval = new TimeSpan(0, 0, 1);
            this.timer.Tick += new EventHandler(TimerTick);
            this.log = new ObservableCollection<ActiveAppLog>() { };
        }

        private void runButton_Click(object sender, RoutedEventArgs e)
        {
            if( timer.IsEnabled)
            {
                timer.Stop();
                this.runButton.Content = "Run";
                return;
            }
            timer.Start();
            this.runButton.Content = "Stop";
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            this.log.Clear();
            this.dataGrid.ItemsSource = this.log.Reverse();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            Process p = GetActiveProcess();
            if (this.log.Count == 0 || this.log.Last().Pid != p.Id)
            {
                var log = new ActiveAppLog { DateTime = DateTime.Now, Name = p.ProcessName, Pid = p.Id, Path = p.MainModule.FileName };
                this.log.Add(log);
                this.dataGrid.ItemsSource = this.log.Reverse();
            }
        }

        private static Process GetActiveProcess()
        {
            // アクティブなウィンドウハンドルの取得
            IntPtr hWnd = WinAPI.GetForegroundWindow();
            int id;
            // ウィンドウハンドルからプロセスIDを取得
            WinAPI.GetWindowThreadProcessId(hWnd, out id);
            Process process = Process.GetProcessById(id);
            return process;
        }
    }

    public class ActiveAppLog
    {
        public DateTime DateTime { get; set; }
        public string Name { get; set; }
        public int Pid { get; set; }
        public string Path { get; set; }
    }

    public class WinAPI
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);
    }
}
